#!/usr/bin/env python3

#
# call this function and give the HTML file saved from IMGT
# This will extract the "green" V-region from the sequences
# date: 2020.06.15 (any update of the way the website display the data may break the function)
#

import sys
import re
SEQS=dict()
with open( sys.argv[1], encoding="ISO-8859-1" ) as FILE: #"utf-8"
	LINES=FILE.readlines()
	SAVE=False
	for LINE in LINES:
		LINE=LINE.strip()
		if SAVE and ID:
			if LINE.endswith("</span>"):
				SAVE=False
			SEQS[ ID ] = SEQS[ ID ]+LINE
		if LINE.startswith(">"):
			SAVE=True
			ID=LINE
			if ID not in SEQS:
				SEQS[ ID ] = ""
			else:
				SAVE=False
#print
for ID in SEQS:
	print(ID)
	#first removing all before <span style="background-color:#75f757;">
	MATCH=re.search(r'>', SEQS[ID])
	if MATCH:
		SEQS[ID]=SEQS[ID][ MATCH.end(0): ]
	#removing all before </span>
	MATCH=re.search(r'<', SEQS[ID])#removal: 
	if MATCH:
		SEQS[ID]=SEQS[ID][ :MATCH.start(0) ]
	print( SEQS[ID])

