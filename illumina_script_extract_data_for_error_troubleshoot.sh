#!/bin/bash

if [ $# -eq 0 ] ; then 
	
	echo "Usage: $(basename $0) source target"; exit 1 ; 
	echo "Advice: use as an output folder the run date and _troubleshooting"
	echo "Don't forget to zip the folder"
fi

SOURCE=${1}
TARGET=${2}

if [ ! -e ${SOURCE} ]; then echo "Error. Source directory does not exists $SOURCE"; exit 1 ; fi
if [ ! -d ${SOURCE} ]; then echo "Error. Source is not a directory $SOURCE"; exit 1 ; fi
if [ -e ${TARGET} ]; then echo "Error. Target directory exists $TARGET"; exit 1 ; fi

mkdir -p ${TARGET}
cp -v ${SOURCE}/RunInfo.xml ${TARGET}
cp -v ${SOURCE}/runParameters.xml ${TARGET}
cp -v ${SOURCE}/SampleSheet.csv ${TARGET}
echo "${SOURCE}/InterOp/ ${TARGET}/InterOp/"
cp -r ${SOURCE}/InterOp/ ${TARGET}/InterOp/

mkdir -p ${TARGET}/Thumbnail_Images/L001/
echo "${SOURCE}/Thumbnail_Images/L001/C1.1/ ${TARGET}/Thumbnail_Images/L001/"
cp -r ${SOURCE}/Thumbnail_Images/L001/C1.1/ ${TARGET}/Thumbnail_Images/L001/

echo -e "\nDone, your directory is ready at ${TARGET}"
