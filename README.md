# Description

Collection of useful function for bioinformaticien.

Useful to manipulate fasta, fastq files, bind files together using matches of columns summarize files headers, generate combinations, calculate shannon entropy index, reverse complement DNA...

Function generally contain a help option.
You can call display the help using -h or --help argument. Some function also display the help when no arguments are given
Several fonction are designed to accept piped data, for example: 
`echo -e "a\t10\nb\t20\nc\t30" | shannon -mode 2col`

# Installation

Please be aware that those function were build on a linux mint operating system. They are designed to be called from a terminal.

Go in the directory where you want to have the functions, and execute on your terminal the following command:
`git clone https://GildasLepennetier@bitbucket.org/GildasLepennetier/general_tools.git`

Alternatively, you can download the package for the bitbucket website, and extract it on the desired folder 

It is possible to change the name of the top folder from general_tools to something more fitting your taste

The executions rights should be conserved, otherwise just add those rights using:
`chmod +x function_name`


You have the general_tool folder at the address /disk1/mystuff/general_tools
To call those functions from the terminal directly, without using the path, you will have to edit your $HOME/.bashrc file:
`nano $HOME/.bashrc`
and add the following line at the end of the file
`PATH=$PATH:/disk1/mystuff/general_tools`
then save the file. To be able to call the function directly after that, you have to apply those change. The easiest way is to execute
`bash`
Otherwise, any new terminal you will open should have the commandes availables.

# Dependencies

Some functions are build in python
You will need at least python 3, but several function should be compatible with python 2.7.
packages to have:
* os,sys,argparse,re,itertools,random (should be there by default)


Some function are build in bash. I am not aware of anyone using old version of bash, just use the last version possible

Some packages will be required, but I designed the function to be as independant as possible.

(TO DO : list of packages dependencies)

# License:
Free to use for non-commercial purposed, with citation.

# Bug
Feel free to contact me in case of bug, incompatibilities, to obtain more information about some functions, or to propose new tools or improvements!

