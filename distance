#!/usr/bin/env python

# Gildas Lepennetier script

# Algo levenshtein from wiki and from:
# Christopher P. Matthews
# christophermatthews1985@gmail.com
# Sacramento, CA, USA


### ### multiprocessing improvement to make the function on the large list.

import sys,os,argparse

METHODES=['lev','ham']

def levenshtein(s, t):
		"""Return minimum number of single-character edits (insertions, deletions or substitutions) required to change one word into the other."""
		if s == t: return 0
		elif len(s) == 0: return len(t)
		elif len(t) == 0: return len(s)
		v0 = [None] * (len(t) + 1)
		v1 = [None] * (len(t) + 1)
		for i in range(len(v0)):
			v0[i] = i
		for i in range(len(s)):
			v1[0] = i + 1
			for j in range(len(t)):
				cost = 0 if s[i] == t[j] else 1
				v1[j + 1] = min(v1[j] + 1, v0[j + 1] + 1, v0[j] + cost)
			for j in range(len(v0)):
				v0[j] = v1[j]
		return v1[len(t)]

def hamming(s, t):
    """Return the Hamming distance between equal-length sequences"""
    if len(s) != len(t):
        raise ValueError("Undefined for sequences of unequal length")
    return sum(el1 != el2 for el1, el2 in zip(s, t))


parser = argparse.ArgumentParser(description="Calculate the distance between sequences list or column.",epilog='Author: Gildas Lepennetier')

parser.add_argument('-i', type=str,help='Provide here an input file. This option ignore sequences given in terminal, if any')
parser.add_argument('-c','-col',type=int,default=1,help='If the input file is tab-separated, use this column. Default first column')

parser.add_argument('-header',action='store_true',help='If you have a header in yout file, you need this flag to skip it.')


parser.add_argument('-m','-method', type=str,default="lev",help='Method of distance to use (ham: hamming, lev: levenshtein) (default: lev)')

parser.add_argument('--avoid_length_error',action='store_true',help='For hamming method: if the length of the sequence is not the same, print NA')

parser.add_argument('-I','--case-insensitive',action='store_true',help='Ignore letter case (conversion to lower case)')

parser.add_argument('-n','--no-duplicate',action='store_true',help='Remove duplicate when existing')

parser.add_argument('-M','--mode',default='FIRST_IS_REF',help='Mode of computation. Default: FIRST_IS_REF. Also available: ALL_PAIRES')

parser.add_argument('--with-seqs',action='store_true',help='Print the sequences in output (default: just the distance)')
parser.add_argument('--version', action='version', version='%(prog)s 2018')#version display
parser.add_argument('--copy',action='store_true',help='Display Copyright informations')
parser.add_argument('--author',action='store_true',help='Display author informations')
parser.add_argument('--verbose', '-v', action='count',default=0,help='add flag(s) to increase verbosity')# count the level of verbosity, +1 for each -v flag

parser.add_argument('seqs',nargs=argparse.REMAINDER,help='List of sequences')

args=vars(parser.parse_args())

if args['author']:
	print ("LEPENNETIER Gildas - gildas.lepennetier@hotmail.fr")
	exit()
if args['copy']:
	print ("Copyright 2018 LEPENNETIER Gildas")
	exit()

if args['m'] not in METHODES:
	sys.stderr.write("ERROR: distance method not recognized. Available: %s\n"%(', '.join(METHODES)))
	exit(1)

if args['verbose'] > 0:
	sys.stderr.write( "loaded: %s sequences\n"%( len(args['seqs'] ) ) )


if args['i']:
	if not os.path.exists(args['i']):
		sys.stderr.write("Error: file not found at %s\n"%(args['i']))
		exit(1)
	
	args['seqs']=[]
	
	with open(args['i'], 'r') as INFILE:
		i=0
		while True:
			rline=INFILE.readline()
			i+=1
			if args['header'] and i==1:
				continue
			if not rline:
				break
			args['seqs'].append(  rline.split('\n')[0].split('\t')[ args['c']-1 ]  )


if args['mode'] == "FIRST_IS_REF":
	REF=args['seqs'].pop(0) #use the 0 to get the first
	if args['verbose'] > 1:
		sys.stderr.write("Reference is: %s\n"%(REF))
	
	
	for TARGET in args['seqs']:
		
		##################
		if args['case_insensitive']:
			REF=REF.lower()
			TARGET=TARGET.lower()
		
		
		if args['no_duplicate']:
			if REF == TARGET:
				if args['verbose'] > 0:
					sys.stderr.write("Duplicate found and skipped.\n")
				continue
			
		if args['m'] == "lev":
			VALUE=levenshtein(REF,TARGET)

		if args['m'] == "ham":
			if args['avoid_length_error'] and len(REF) != len(TARGET):
				VALUE="NA"
			else:
				VALUE=hamming(REF,TARGET)
		
		if args ['with_seqs']:
			OUT="%s\t%s\t%s"%(REF,TARGET,VALUE)
		else:
			OUT="%s"%(VALUE)
		print(OUT)
		
		##################

if args['mode'] == "ALL_PAIRES":
	for i in range(0,len(args['seqs'])):
		for j in range(i+1,len(args['seqs'])):
			
			REF=args['seqs'][i]
			TARGET=args['seqs'][j]
			
			
			if args['no_duplicate']:
				if REF == TARGET:
					if args['verbose'] > 0:
						sys.stderr.write("Duplicate found and skipped.\n")
					continue
			
			##################
			if args['case_insensitive']:
				REF=REF.lower()
				TARGET=TARGET.lower()
			if args['m'] == "lev":
				VALUE=levenshtein(REF,TARGET)
			if args['m'] == "ham":
				if args['avoid_length_error'] and len(REF) != len(TARGET):
					VALUE="NA"
				else:
					VALUE=hamming(REF,TARGET)
				
			if args ['with_seqs']:
				OUT="%s\t%s\t%s"%(REF,TARGET,VALUE)
			else:
				OUT="%s"%(VALUE)
			print(OUT)
			
			##################

