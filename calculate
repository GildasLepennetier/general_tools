#!/usr/bin/env python
import sys,argparse,re
import statistics

parser = argparse.ArgumentParser(description="Take from input, one numerical value per line, and return the result of the specified command -c [using two time -c result in only the last -c command executed]",epilog='Author: Gildas Lepennetier')
parser.add_argument('-i', type=argparse.FileType('r'),default=sys.stdin, help='input file name, can take stdin')
parser.add_argument('-o', type=argparse.FileType('w'),default=sys.stdout, help='output file name, or stdout')
parser.add_argument('-e', type=argparse.FileType('w'),default=sys.stderr, help='error, log file, or stderr')
parser.add_argument('-c', type=str,default='mean_arithmetic', help='calculate using this function (default=mean_arithmetic). Use -INFO for more functions')
parser.add_argument('-s', type=str,default=',', help='separator (e.g. separate elements for the list of positions)')
parser.add_argument('-p', type=str,help='pattern to search for. Python regexp. Return position of the pattern')

parser.add_argument('-C', type=int,default=1,help='Column to use (default 1)')
parser.add_argument('-F', type=str,default="\t",help='column Field delimiter (default tab)')
parser.add_argument('-P',action="store_true",help='Print all columns, add the computed value at the end')

parser.add_argument('-round',type=int,default=2,help='digits to round the percentage')

parser.add_argument('-maxoutlen',type=int,default=0,help='limit for output length for the positions (default: 0)')
parser.add_argument('-INFO',action="store_true",help='Type the to display the different commands.')
parser.add_argument('-header',action="store_true",help='To skip the first line if you have a header.')

args=vars(parser.parse_args())

if args['c'] not in ['min','max','median','std','sum','mean','mean_arithmetic','mean_geometric','percent','positions']:
	if 'count_' not in args['c']:
		args['e'].write("ERROR function not recognized, or not available: %s\n"%(args['c']))
		exit(1)

if args['p'] and not args['c'] == 'positions':
	args['e'].write("ERROR: you are using a pattern without the appropriate commande: either useless pattern or command error\n")
	exit(1)

if args['INFO']:
	args['e'].write("\tmin\t\treturn the minimum value of the list\n")
	args['e'].write("\tmax\t\treturn the maximum value of the list\n")
	args['e'].write("\tmedian\t\treturn the median value of the list\n")
	args['e'].write("\tstd\t\treturn the standard deviation of the list\n")
	args['e'].write("\tsum\t\treturn the sum of the list\n")
	
	args['e'].write("\tmean\treturn the average of the list (=mean_arithmetic)\n")
	args['e'].write("\tmean_arithmetic\treturn the arithmetic average of the list\n")
	args['e'].write("\tmean_geometric\treturn the geometric average of the list\n")
	
	args['e'].write("\tpercent\t\treturn the percentage of each line using the total of the given list\n")
	args['e'].write("\tpositions\treturn a list of position per line, based on the gien pattern\n")
	
	args['e'].write("\tcount_CMD_VALUE\tcount the number of lines.\n\t\tReplace CMD by on of ['eq', 'le', 'ge', 'gt', 'lt' ] (greater than, lower than, equal, greater or equal)\n\t\tReplace VALUE by your value, can be float with '.'\n")
	
	exit()

LIST=[]		#list of interesting values
if args['P']:
	LINES=[]	#list of lines

def mean_arithmetic(n):
	return float(sum(n)) / max(len(n), 1)
def median(lst):
	lst = sorted(lst)
	n = len(lst)
	if n < 1:
			return None
	if n % 2 == 1:
			return lst[n//2]
	else:
			return sum(lst[n//2-1:n//2+1])/2.0
def geometric_mean(nums):
	return (reduce(lambda x, y: x*y, nums))**(1.0/len(nums))

# load the lines in a list
if True:
	i=0
	for rline in args['i']:
		i+=1
		if args['header'] and i == 1:
			continue
		
		lineSplit=rline.split('\n')[0].split( args['F'] )
		line=lineSplit[ args['C']-1 ]
		
		if args['P']:
			LINES.append(lineSplit)
		
		if args['c'] == 'positions': #in this case we want the string
			LIST.append(line)
		else: #all other cases are floats
			try:
				LIST.append(float(line))
			except ValueError:
				args['e'].write("ERROR line %s not numerical\n"%(i))
				if i == 1:
					args['e'].write("you very probably have a header... shipped auto\n")

# NOTE : function that is not apply line per line (no option -P)

if args['c'] == 'mean_arithmetic' or args['c'] == 'mean':
	args['o'].write("%s\n"%( mean_arithmetic(LIST) ))
	exit()
	
if args['c'] == 'mean_geometric':
	args['o'].write("%s\n"%( geometric_mean(LIST) ))
	exit()
	
if args['c'] == 'min':
	args['o'].write("%s\n"%( min(LIST) ))
	exit()
	
if args['c'] == 'max':
	args['o'].write("%s\n"%( max(LIST) ))
	exit()
	
if args['c'] == 'median':
	args['o'].write("%s\n"%( median(LIST) ))
	exit()
	
if args['c'] == 'sum':
	args['o'].write("%s\n"%( sum(LIST) ))
	exit()
	
if args['c'] == 'std':
	args['o'].write("%s\n"%( statistics.stdev(LIST) ))
	exit()




# NOTE : functions that are apply line per line (option -P is possible to print columns)

#make the percentage of each line compared to the total of the column (which is 100%)
if args['c'] == 'percent':
	SUM=sum(LIST)
	
	for index in range( len(LIST) ):
		nb=LIST[index]
		OUT="%s"%( round( float(nb)/SUM*100 , args['round'] ) )
		if args['P']:
			args['o'].write(   args['F'].join( LINES[index] + [str(OUT)] ) + "\n")
		else:
			args['o'].write(OUT+"\n")
	exit()

# find position of pattern, return positions
if args['c'] == 'positions':
	
	for index in range( len(LIST) ):
		pattern=args['p']
		positions=args['s'].join( [ str(m.start()+1) for m in re.finditer( pattern , LIST[index] ) ])
		if len(positions) >= args['maxoutlen'] and args['maxoutlen'] != 0:
			positions = positions[ : args['maxoutlen'] ] #trimm output
			args['e'].write("WARNING: maximum output length per line reached (%s) -> the line will be trimmed\n"%(args['maxoutlen']))
		if args['P']:
			args['o'].write(   args['F'].join( LINES[index] + [str(positions)] ) + "\n")
		else:
			args['o'].write("%s\n"%(positions))
		
		
	exit()

# NOTE : function that is not apply line per line (no option -P)

if 'count_' in args['c']:
	SPLIT=args['c'].split("_")
	CMD=SPLIT[1]
	if CMD not in [ 'eq', 'le', 'ge', 'gt', 'lt' ]:
		args['e'].write("Error: not recognized as command for the count_ : %s \n"%(CMD))
		exit(1)
	try:
		VALUE=float(SPLIT[2])
	except ValueError:
		args['e'].write("Error: cannot be converted to float: %s\n"%(SPLIT[2]))
		exit(1)
	
	x=0
	if CMD == 'eq':
		for el in LIST:
			if el == VALUE: x+=1
	if CMD == 'le':
		for el in LIST:
			if el <= VALUE: x+=1
	if CMD == 'ge':
		for el in LIST:
			if el >= VALUE: x+=1
	if CMD == 'gt':
		for el in LIST:
			if el > VALUE: x+=1
	if CMD == 'lt':
		for el in LIST:
			if el < VALUE: x+=1
	
	args['o'].write("%s\n"%(x))
	exit()
