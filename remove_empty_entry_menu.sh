#!/bin/bash
set -e

#https://askubuntu.com/questions/40884/how-can-i-remove-orphaned-start-menu-entries

### trash-put is a safe was to remove files: 
#sudo apt install trash-cli

### you usually do not want to deal with root entry
#for i in {/usr,~/.local}/share/applications/*.desktop; do which $(grep -Poh '(?<=Exec=).*?( |$)' $i) > /dev/null || trash-put $i; done

for i in ~/.local/share/applications/*.desktop; do which $(grep -Poh '(?<=Exec=).*?( |$)' $i) > /dev/null || trash-put $i; done
