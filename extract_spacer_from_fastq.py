#!/usr/bin/env python
import sys,argparse
parser = argparse.ArgumentParser(description="From a fastq file, extract the spacer part of a UMI tag",epilog='Author: Gildas Lepennetier')
parser.add_argument('-i', type=argparse.FileType('r'),default=sys.stdin, help='input file name, can take stdin')
parser.add_argument('-o', type=argparse.FileType('w'),default=sys.stdout, help='output file name, or stdout')
parser.add_argument('-e', type=argparse.FileType('w'),default=sys.stderr, help='output file name, or stdout')
parser.add_argument('-l','--umiLen', type=int,default=15,help='length of the UMI, default 15 nt')
parser.add_argument('-n','--tagName',type=str,default="BARCODE",help='Name of the tag, default is BARCODE')
parser.add_argument('-a','--equal',  type=str,default="=",help='separation between tag and value, default is =')
parser.add_argument('-s','--tagSep', type=str,default="|",help='separation between tags, default is |')


parser.add_argument('-noID', action='store_true',help='do not print sequence ID')
parser.add_argument('-noLen', action='store_true',help='do not print spacer length')

args=vars(parser.parse_args())

i=0
while True:
	rline=args['i'].readline()
	if not rline: break
	i+=1
	if i % 4 == 1:
		try:
			line = rline.strip().split(args['tagSep'])
			OUT=[]
			
			if not args['noID']:OUT.append(line[0])
			
			for el in line:
				EL=el.split(args['equal'])
				if EL[0]==args['tagName']:UMI_seq=EL[1]
			SPACER=UMI_seq[args['umiLen']:]
			
			OUT.append(SPACER)
			
			if not args['noLen']:OUT.append(str(len(SPACER)))
			
			args['o'].write(  "\t".join(OUT) + "\n")
			
		except IOError:
			break # to avoid IO error when piping in head for example

