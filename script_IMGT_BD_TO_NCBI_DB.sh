#!/bin/bash
set -e

# NOTE : http://www.imgt.org/vquest/refseqh.html

SEQKIT=/media/ga94rac/DISK4/TOOLS_2/SEQKIT/seqkit
CMD_FASTA_TOOLKIT=/home/ga94rac/presto_cluster/PROGRAMS/general_tools/fasta_toolkit


NEW_DB_DIR=/media/ga94rac/DISK4/TOOLS_2/IMGT_DB_igblastn/all/
mkdir -p $NEW_DB_DIR
rm -f $NEW_DB_DIR/*


### Make DB for human
echo "concat sequences for human"
SPECIES=Homo_sapiens
SOURCE_PATH=/media/ga94rac/DISK4/TOOLS_2/IMGT_DB/IMGT_V-QUEST_reference_directory/$SPECIES/
### BCR
cat $SOURCE_PATH/IG/IGHD.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_D.fasta
cat $SOURCE_PATH/IG/IG{H,K,L}J.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_J.fasta
cat $SOURCE_PATH/IG/IG{H,K,L}V.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_V.fasta
### TCR
cat $SOURCE_PATH/TR/TR{A,B,D,G}J.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_J.fasta
cat $SOURCE_PATH/TR/TR{A,B,D,G}V.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_V.fasta
cat $SOURCE_PATH/TR/TR{B,D}D.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_D.fasta




echo "concat sequences for mouse --- careful: there are some mus spretus annotations"
SPECIES=Mus_musculus
SOURCE_PATH=/media/ga94rac/DISK4/TOOLS_2/IMGT_DB/IMGT_V-QUEST_reference_directory/$SPECIES/
### BCR
cat $SOURCE_PATH/IG/IGHD.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_D.fasta
cat $SOURCE_PATH/IG/IG{H,K,L}J.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_J.fasta
cat $SOURCE_PATH/IG/IG{H,K,L}V.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.IG_V.fasta
### TCR
cat $SOURCE_PATH/TR/TR{A,B,D,G}J.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_J.fasta
cat $SOURCE_PATH/TR/TR{A,B,D,G}V.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_V.fasta
cat $SOURCE_PATH/TR/TR{B,D}D.fasta | sed -r '/^\s*$/d' > $NEW_DB_DIR/$SPECIES.TR_D.fasta


if [ 0 ]; then echo -e "\nremove Mus spretus sequences"
	COUNT=0
	TOTAL_BEFORE=$( grep ">" $NEW_DB_DIR/*.fasta | wc -l )
	for FILE in $( ls $NEW_DB_DIR/*.fasta); do
		OUT_IDS=$NEW_DB_DIR/$(basename $FILE).list_id_mus_spretus.txt
		cat "$FILE" | grep -i "Mus spretus" |sed 's/>//g' > $OUT_IDS
		if [ -s $OUT_IDS ];then
			COUNT=$(( $COUNT + $(cat $OUT_IDS |wc -l ) ))
			echo "WARNING: MUS SPRETUS IN MUSCULUS: $OUT_IDS"
			echo "example top 10:"
			head -n 10 $OUT_IDS
			$CMD_FASTA_TOOLKIT -cmd fa_rm_ids -in "$FILE" -out "$FILE".rmids -id_file $OUT_IDS
			mv "$FILE".rmids "$FILE"
		else
			rm $OUT_IDS
		fi
	done
	TOTAL_AFTER=$( grep ">" $NEW_DB_DIR/*.fasta | wc -l )
	echo "$COUNT 'Mus spretus' sequences removed in total"
	echo "(TOTAL_BEFORE=$TOTAL_BEFORE ; TOTAL_AFTER=$TOTAL_AFTER ; diff = $(( $TOTAL_BEFORE - $TOTAL_AFTER )))"
fi



cd $NEW_DB_DIR
for FILE in $(ls *.fasta); do 
	
	echo "removing duplicate using $SEQKIT rmdup"
	FILE_NO_DUP="${FILE%.fasta}".rmdup.fasta
	$SEQKIT rmdup -s -i --dup-seqs-file "$FILE".dupSEQS.txt -o "$FILE_NO_DUP" $FILE
	
	FILE_DB=${FILE_NO_DUP%.fasta}.ncbi.fasta
	echo "edit_imgt_file.pl on $FILE_NO_DUP"
	perl /media/ga94rac/DISK4/TOOLS_2/ncbi-igblast-1.14.0/bin/edit_imgt_file.pl $FILE_NO_DUP > $FILE_DB
	
	echo "makeblastdb on $FILE_DB"
	makeblastdb -parse_seqids -dbtype nucl -in $FILE_DB
done


if [ 0 ];then echo "create the -auxiliary_data FILE"
	
	OUTFILE=Homo_sapiens.aux
	rm -f "$OUTFILE"
	#take BCR and TCR all files
	#Should be the IMGT annotated files
	for FILE in $(ls Homo_sapiens.*.rmdup.fasta); do
			# WARNING: positions are 0-based
			#The chain type, first coding frame start position, chain type, CDR3 stop.
			# printing the substring to get the type and chaine for field 2
			# no CDR3 stop , leaving empty
		cat "$FILE" | grep "^>" | awk -F'|' '{print $2 "\t" $8-1 "\t" substr($2,4,1) substr($2,3,1) "\t" }' | sort >> "$OUTFILE"
	done
	echo "$(pwd)/$OUTFILE"
	
	OUTFILE=Mus_musculus.aux
	rm -f "$OUTFILE"
	#take BCR and TCR all files
	#Should be the IMGT annotated files
	for FILE in $(ls Mus_musculus.*.rmdup.fasta); do
			# WARNING: positions are 0-based
			#The chain type, first coding frame start position, chain type, CDR3 stop.
			# printing the substring to get the type and chaine for field 2
			# no CDR3 stop , leaving empty
		cat "$FILE" | grep "^>" | awk -F'|' '{print $2 "\t" $8-1 "\t" substr($2,4,1) substr($2,3,1) "\t" }' | sort >> "$OUTFILE"
	done
	echo "$(pwd)/$OUTFILE"
	
fi





echo -e "\nMUS SPRETUS SEQUENCES:"
find $NEW_DB_DIR -name "*.list_id_mus_spretus.txt"

echo -e "\nDUPLICATED SEQUENCES:"
find $NEW_DB_DIR -name "*.dupSEQS.txt"



echo "done"
