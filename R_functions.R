#save(dds, file = "rsave_dds")
#save.image()
#load(file = "rsave_dds")
#load(".RData") 

# Korn style - 1
plot_pdf_volcano=function(res,fileout,geneToColor=c(),printGenesNames=T,geneCol=c(),toupperCase=T,
						  PCH=19,XLIM=c(-10,10),XBY=5,YLIM=c(-80,0),YBY=20,
						  cex_points=2.2,cex_color=cex_points ){
	
	fileout= paste0( gsub(pattern = ".pdf",replacement = "",x = fileout), ".pdf")
	print(paste0("saving pdf: ",fileout,"at:",getwd()))
	pdf(file = fileout, width = 10, height = 10,useDingbats=F)
	par( mar=c(10,10,10,10))
	plot( x = res$log2FoldChange, y = log10(res$pvalue), xlim=XLIM, ylim=YLIM, xaxt='n',yaxt='n',xaxs="i",yaxs="i",
		  las=1, cex=cex_points, pch=PCH, xlab="", ylab="" , cex.lab=2.5, cex.axis=2.5)
	#axis labels
	mtext(text="log10(p-value)", side=2,cex=2.5,padj=-4)
	mtext(text="log2 FC", side=1,cex=2.5,padj=3)
	#axis thick
	axis(side = 1, at = seq(XLIM[1],XLIM[2],XBY), labels = seq(XLIM[1],XLIM[2],XBY), tick = T, lwd = 2, cex.lab=2.5, cex.axis=2.5, padj=.5)
	axis(side = 2, at = seq(YLIM[1],YLIM[2],YBY), labels = seq(YLIM[1],YLIM[2],YBY), tick = T, lwd = 2, cex.lab=2.5, cex.axis=2.5, padj=.5, las=1)
	#consistant line width
	abline(h = YLIM[2], lwd=2)
	abline(v = XLIM[2], lwd=2)
	#limits of signif for p-value and FC
	abline( h=log10(0.05), lty=2, col="gray")
	abline( v=2, lty=2, col="gray")
	abline( v=-2, lty=2, col="gray")
	
	
	#highlight some genes on the list
	if(length(geneToColor) >0){
		if(length(geneToColor) != length(geneCol)){
			print("Error: geneToColor not same length as geneCol")
			return(-1)
			}
		if(toupperCase){
			rownames(res) = toupper(rownames(res))
			geneToColor=toupper(geneToColor)
		}
		MATCHES1=rownames(res) %in% geneToColor
		MATCHES2=geneToColor %in% rownames(res)
		
		if ( sum(MATCHES1) > 0 ){
			
			print(paste(length(geneToColor),"genes to highlight."))
			print(paste(sum(MATCHES1),"genes matching."))
			#print(paste(sum(MATCHES2),"genes matching."))
			
			points(x = res$log2FoldChange[ MATCHES1 ] , 
				   y = log10(res$pvalue[ MATCHES1 ]) ,
				   col=geneCol[ MATCHES2 ], cex=cex_points, pch=PCH)
			if(printGenesNames){
				#It is possible to select the list of gene to actually display
				LABELS=rownames(res)[ MATCHES1 ]
				text(x = res$log2FoldChange[ MATCHES1 ], 
					 y = log10(res$pvalue[ MATCHES1 ]), 
					 labels = LABELS )
			}
		}else{
			print("WARNING: 0 genes present in this subset match the list of gene to highlight")
		}
	}
	
	#print on top right and left the number of genes that are FC + signif and FC - signif
	
	dev.off()
}

####### from tutorial
volcanoplot <- function (res, lfcthresh=2, sigthresh=0.05, main="Volcano Plot", legendpos="bottomright", labelsig=TRUE, textcx=1, ...) {
	with(res, plot(log2FoldChange, -log10(pvalue), pch=20, main=main, ...))
	with(subset(res, padj<sigthresh ), points(log2FoldChange, -log10(pvalue), pch=20, col="red", ...))
	with(subset(res, abs(log2FoldChange)>lfcthresh), points(log2FoldChange, -log10(pvalue), pch=20, col="orange", ...))
	with(subset(res, padj<sigthresh & abs(log2FoldChange)>lfcthresh), points(log2FoldChange, -log10(pvalue), pch=20, col="green", ...))
	if (labelsig) {
		require(calibrate)
		with(subset(res, padj<sigthresh & abs(log2FoldChange)>lfcthresh), textxy(log2FoldChange, -log10(pvalue), labs=Gene, cex=textcx, ...))
	}
	legend(legendpos, xjust=1, yjust=1, legend=c(paste("FDR<",sigthresh,sep=""), paste("|LogFC|>",lfcthresh,sep=""), "both"), pch=20, col=c("red","orange","green"))
}
maplot <- function (res, thresh=0.05, labelsig=TRUE, textcx=1, ...) {
	with(res, plot(baseMean, log2FoldChange, pch=20, cex=.5, log="x", ...))
	with(subset(res, padj<thresh), points(baseMean, log2FoldChange, col="red", pch=20, cex=1.5))
	if (labelsig) {
		require(calibrate)
		with(subset(res, padj<thresh), textxy(baseMean, log2FoldChange, labs=Gene, cex=textcx, col=2))
	}
}

#For this, see terrUtils
rowVars <- function(x, na.rm=FALSE, dims=1, unbiased=TRUE,SumSquares=FALSE, twopass=FALSE) {
	if (SumSquares) return(rowSums(x^2, na.rm, dims))
	N <- rowSums(!is.na(x), FALSE, dims)
	Nm1 <- if (unbiased) N-1 else N
	if (twopass) {x <- if (dims==0) x - mean(x, na.rm=na.rm) else
		sweep(x, 1:dims, rowMeans(x,na.rm,dims))}
	(rowSums(x^2, na.rm, dims) - rowSums(x, na.rm, dims)^2/N) / Nm1
} 

plotUpDownSigGenes <- function(results, colNums, rld, title) {
	
	# make the lists
	upgenes <- rownames(head(results[ order( results$log2FoldChange ), ], n=20))
	downgenes <- rownames(head(results[ order( -results$log2FoldChange ), ], n=20))
	
	# this gives us the rows we want
	rows <- match(upgenes, row.names(rld))
	mat <- assay(rld)[rows,colNums]
	mat <- mat - rowMeans(mat)
	
	# the labels are hard coded at the moment :(
	df <- as.data.frame(colData(rld)[c("labelA","labelB")])
	pheatmap(mat, fontsize=5, annotation_col=df, main=paste(title,"top 20 up genes"))
	
	# this gives us the rows we want
	rows <- match(downgenes, row.names(rld))
	mat <- assay(rld)[rows,colNums]
	mat <- mat - rowMeans(mat)
	
	df <- as.data.frame(colData(rld)[c("labelA","labelB")])
	pheatmap(mat, fontsize=5, annotation_col=df, main=paste(title,"top 20 down genes"))
}

# utilitaires
vector_to_color=function(x, colors=rainbow(length(unique(x))), default="black", alpha=1){
	require(grDevices)
	if( length(colors) < length(unique(x))){
		print(paste("Error: only",length(colors),"colors provided, but required",length(unique(x))))
		print("Appying default color selection using rainbow")
		colors=rainbow(length(unique(x)))
	}
	y=rep(default, length(x))
	for( index in 1:length(unique(x))){y [ x == unique(x)[index] ] = adjustcolor( colors[index] , alpha.f =alpha)}
	return(y)
}
pair_map=function(vect1, vect2,ignore_case=F,check_dupli=T,no_self=T){
	V1=c()
	V2=c()
	if(ignore_case){
		vect1=toupper(vect1)
		vect2=toupper(vect2)
	}
	if(check_dupli){
		vect1=unique(vect1)
		vect2=unique(vect2)
	}
	for (index1 in 1:(length(vect1)-1) ){
		for ( index2 in ((index1):length(vect2))){
			if(no_self){
				if (index1 != index2){
					V1=c(V1,vect1[ index1])
					V2=c(V2,vect2[ index2])
				}
			}else{
				V1=c(V1,vect1[ index1])
				V2=c(V2,vect2[ index2])
			}
			
		}
	}
	return(list(index1=V1,index2=V2,names1=vect1,names2=vect2))
	"This function takes two list of value and make the comparisons 2 by 2"
}

#the opposite : gdata::humanReadable
read.gmt = function(filepath){
	con = file(filepath, "r")
	GENES_LIST=list()
	while ( T ) {
		rline = readLines(con, n = 1)
		if(length(rline)==0){break}
		
		line=strsplit(rline,split = "\t")
		PATHWAY_NAME = line[[1]][1]
		GENES = line[[1]][ c(3:length(line[[1]])  ) ]
		GENES_LIST [[ PATHWAY_NAME ]] = GENES
	}
	close(con)
	return(GENES_LIST)
}


