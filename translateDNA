#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys,argparse
parser = argparse.ArgumentParser(description="Translate DNA into amino acids. stop codon = _ ; unknown/uncomplete codons = ?",epilog='Author: Gildas Lepennetier')

parser.add_argument('-err', type=argparse.FileType('w'),default=sys.stderr, help='error file name, otherwise print in stderr')
parser.add_argument('-out', type=argparse.FileType('w'),default=sys.stdout, help='output file name, otherwise print in stdout')
parser.add_argument('-kg', '--keep-going',action='store_true',default=False,help='Keep translating even after a stop codon is reached')
# to do : species-specific codons

parser.add_argument('-V','--version', action='version', version='%(prog)s 04-28-2014')#version display
parser.add_argument('-C','--copy',action='store_true',help='Display Copyright informations')
parser.add_argument('-A','--author',action='store_true',help='Display author informations')
parser.add_argument('-q','--quiet',action='store_true',help='to remove the error messages')

known_args, seqs = parser.parse_known_args()
args = vars( known_args )

if args['author']:
    print ("LEPENNETIER Gildas - gildas.lepennetier at hotmail.fr")
    exit()
if args['copy']:
    print ("Copyright 2019 LEPENNETIER Gildas")
    exit()


codons_table = {
    'ATA':'I', 'ATC':'I', 'ATT':'I', 'ATG':'M',
    'ACA':'T', 'ACC':'T', 'ACG':'T', 'ACT':'T',
    'AAC':'N', 'AAT':'N', 'AAA':'K', 'AAG':'K',
    'AGC':'S', 'AGT':'S', 'AGA':'R', 'AGG':'R',
    'CTA':'L', 'CTC':'L', 'CTG':'L', 'CTT':'L',
    'CCA':'P', 'CCC':'P', 'CCG':'P', 'CCT':'P',
    'CAC':'H', 'CAT':'H', 'CAA':'Q', 'CAG':'Q',
    'CGA':'R', 'CGC':'R', 'CGG':'R', 'CGT':'R',
    'GTA':'V', 'GTC':'V', 'GTG':'V', 'GTT':'V',
    'GCA':'A', 'GCC':'A', 'GCG':'A', 'GCT':'A',
    'GAC':'D', 'GAT':'D', 'GAA':'E', 'GAG':'E',
    'GGA':'G', 'GGC':'G', 'GGG':'G', 'GGT':'G',
    'TCA':'S', 'TCC':'S', 'TCG':'S', 'TCT':'S',
    'TTC':'F', 'TTT':'F', 'TTA':'L', 'TTG':'L',
    'TAC':'Y', 'TAT':'Y', 'TAA':'_', 'TAG':'_',
    'TGC':'C', 'TGT':'C', 'TGA':'_', 'TGG':'W',
    }

def translateDNA(seq, BreakAtFirstStopCodon=True, quiet=False):
	if seq:
		if len(seq) % 3 != 0 and not quiet:
			args['err'].write("Warning: sequence length (%s) not multiple of 3.\n"%(len(seq)))
		for codon_start in range(0, len(seq), 3):
			codon=seq[codon_start:codon_start+3]#get codon
			CODON=codon.upper()#convert to upper
			try:
				AminoAcide=codons_table[ CODON ]#get AA
				if not codon.isupper(): AminoAcide=AminoAcide.lower() #if original codon is lower case, convert to lower case
				if AminoAcide:args['out'].write("%s"%(AminoAcide))
				if BreakAtFirstStopCodon and AminoAcide == "_": break
			except KeyError:
				args['out'].write ("?")
	else:
		if not quiet:
			args['err'].write ("Warning: empty sequence given\n")
	args['out'].write("\n")

if __name__ == '__main__':
	
	if args['keep_going']: BreakAtStopCodon=False
	else: BreakAtStopCodon=True
	for seq in seqs:
		translateDNA(seq, BreakAtStopCodon, args['quiet'])