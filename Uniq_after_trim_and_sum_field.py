#!/usr/bin/env python

import sys

if len(sys.argv) != 3:
	sys.stderr.write("Please give some arguments:\n")
	sys.stderr.write("\tusage: <infile.fasta> <outfile.fasta>\n")
	exit()

infile=sys.argv[1]
outfile=sys.argv[2]
sep="|" #field separator
Annotations=["ZMC:sum"]

def Fasta_To_Dico_path(path,idtag='>',eol="\n"):
	with open(path,'r') as file:
		dico={}
		for line in file:
			line=line.split(eol)[0]
			if line[0]==idtag:
				ID=line[1:] #avoid '>' in IDs
				if ID in dico:
					sys.stderr.write("Error in Fasta_To_Dico_path: duplicated ID %s"%(ID))
					exit(-1)
				dico[ID]='' 
			else:
				dico[ID]+=line
	return dico

def get_annotation(name,sep="|",equal="=",first_record="ID"):
	if not isinstance(name, str):
		sys.stderr.write("Error in get_annotation: not a string but a %s\n"%(type(name).__name__))
		exit(-1)
	annotation={}
	index=0
	for element in name.split(sep):
		index+=1
		if index==1:
			annotation[first_record]=element
		else:
			try:
				name=element.split(equal)[0]
				if name == first_record:
					sys.stderr.write("Error: %s already exists in the annotations, please use another name for the first record or modify this record name\n"%(first_record))
					exit(-1)
				value=element.split(equal)[1]
				annotation[name]=value
			except:
				sys.stderr.write("Warning: cannot parse annotations from %s (index %s)\n"%(element,index))
				pass
	return (annotation)

def revert_dico_make_list(dico):
	""" return a dict where the values are used as keys. Keys duplicated when making a set of values are now in a list"""
	dico_rev={}
	for k1 in dico:
		k2 = dico[ k1 ]
		if k2 in dico_rev:
			dico_rev[ k2 ].append( k1 )
		else:
			dico_rev[ k2 ] = [ k1 ]
	return (dico_rev)

def insert_in_string(string,insert="\n",each=80):
	string2=""
	ii=0
	for letter in string:
		ii+=1
		if ii % each == 0 :
			string2=string2+letter
			string2=string2+insert
		else:
			string2=string2+letter
	return string2

DicoIDS  = Fasta_To_Dico_path(infile)
DicoSEQS = revert_dico_make_list(DicoIDS) #reverse to find the duplicated sequences


#sum the field - keep the first
with open(outfile,"w") as FHout:
	for UniqSeq in DicoSEQS:
		index=0
		for ID_of_seq in DicoSEQS[ UniqSeq ]:
			index+=1
			if index==1: #keep first entry as reference
				
				FirstID_dico=get_annotation(ID_of_seq)
				
			else: #sum the annotations
				
				OtherID_dico=get_annotation(ID_of_seq)
				
				for annot in Annotations:
					if annot == "ID": 
						pass
					FieldID=annot.split(':')[0]
					Action=annot.split(':')[1]
					if Action == "sum":
						if FieldID not in FirstID_dico:
							sys.stderr.write("Warning: FieldID '%s' added and set to 1 because missing in the first record (for %s)\n"%(FieldID,ID_of_seq))
							FirstID_dico[FieldID]=1
						# sum the fields, and save in firstID.
						FirstID_dico[ FieldID ] = int(FirstID_dico[ FieldID ]) + int(OtherID_dico[ FieldID ])
			
		#save the output:
		#first save the new ID with fixed annotations
		FHout.write( sep.join( [ ">%s"%( FirstID_dico.pop("ID") ) ] + [  "=".join( [ fieldID,str(FirstID_dico[fieldID]) ] ) for fieldID in FirstID_dico ] ) + "\n")
		#then save the fasta, with 80 nt lines
		FHout.write(  "%s\n"%(UniqSeq) )



