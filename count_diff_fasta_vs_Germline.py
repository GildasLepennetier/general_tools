#!/usr/bin/env python
import sys,argparse
parser = argparse.ArgumentParser(description="Calculate distance between reference and all other sequences.",epilog='Author: Gildas Lepennetier')
parser.add_argument('-in',type=str,required=True,help='first file address: the query')
parser.add_argument('-out',type=str,required=True,help='second file address: the reference')
parser.add_argument('-ref',default="Germline",help='Reference: default=Germline')
parser.add_argument('--verbose', '-v', action='count',default=0,help='add flag(s) to increase verbosity')
args=vars(parser.parse_args())

def Fasta_To_Dico_path(path,idtag='>',eol="\n"):
	with open(path,'r') as file:
		dico={}
		for line in file:
			line=line.split(eol)[0]
			if line[0]==idtag:
				ID=line[1:] #avoid '>' in IDs
				dico[ID]='' 
			else:
				dico[ID]+=line
	return dico
def hamming(s, t):
    """Return the Hamming distance between equal-length sequences"""
    if len(s) != len(t):
        raise ValueError("Cannot calculate hamming distance for sequences of unequal length")
    return sum(el1 != el2 for el1, el2 in zip(s, t))

if args['verbose'] > 1:
	sys.stderr.write("processing: %s\n"%(args['in']))

DICO=Fasta_To_Dico_path( args['in'] )
if args['verbose'] > 1:
	sys.stderr.write("%s sequences loaded\n"%(len(DICO)))

#find germline
GL_ID=""
for key in DICO:
	if key.startswith( args['ref'] ):
		if GL_ID != "":
			sys.stderr.write("ERROR: Duplicated reference '%s': try a more specific name for the reference (%s)\n"%(args['ref'], key))
			exit(-1)
		GL_ID=key
		GL_SEQ=DICO[key]
		#DICO.pop(GL_ID)#remove from list

if GL_ID=="":
	sys.stderr.write("ERROR: No reference '%s' found in %s!\n"%(args['ref'],args['in']))
	exit(-1)

if args['verbose'] > 1:
	sys.stderr.write("reference Id is: %s\n"%(GL_ID))
	
#calculate and save distance
if args['verbose'] > 0:
	sys.stderr.write("saving in: %s\n"%(args['out']))
with open(args['out'], "w") as FHout:
	FHout.write("SeqID\tDistFromRef\n")
	for SeqID in DICO:
		DIFF=hamming(GL_SEQ, DICO[SeqID])
		OUT="\t".join([SeqID, str(DIFF)])
		FHout.write(OUT+"\n")
